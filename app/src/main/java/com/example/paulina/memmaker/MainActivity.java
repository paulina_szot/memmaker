package com.example.paulina.memmaker;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

@EActivity
public class MainActivity extends AppCompatActivity {

    @ViewById(R.id.imageView)
    protected ImageView memView;

    @ViewById(R.id.addImage)
    protected Button addImage;

    @ViewById(R.id.save)
    protected Button save;

    @ViewById(R.id.share)
    protected Button share;

    @ViewById(R.id.reset)
    protected Button reset;

    @ViewById(R.id.topText)
    protected EditText topText;

    @ViewById(R.id.bottomText)
    protected EditText bottomText;

    @ViewById(R.id.topSpinner)
    protected Spinner topSpinner;

    @ViewById(R.id.bottomSpinner)
    protected Spinner bottomSpinner;

    private static final int SELECT_PHOTO = 100;

    private Context context;

    private  File file;

    private Bitmap bitmap;

    private Bitmap currentImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @AfterViews
    protected void afterViews(){
        context = MainActivity.this;
        setListeners();
        addSpinners();
    }

    @Click(R.id.addImage)
    protected void addImage(){
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(context.getString(R.string.image));
        startActivityForResult(intent, SELECT_PHOTO);
    }

    @Click(R.id.reset)
    protected void reset(){
        topSpinner.setSelection(0);
        topText.setText(context.getString(R.string.empty));
        bottomSpinner.setSelection(0);
        bottomText.setText(context.getString(R.string.empty));
        share.setVisibility(View.INVISIBLE);
        memView.setImageResource(R.drawable.noimage);
        bitmap = null;
        currentImage = null;
        file = null;
    }

    @Click(R.id.save)
    protected void save(){
        if(bitmap != null) {
            final Dialog dialog = new Dialog(context);
            dialog.setContentView(R.layout.mem_name);
            Button b = (Button) dialog.findViewById(R.id.buttonOk);
            b.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EditText et2 = (EditText) dialog.findViewById(R.id.textName);
                    String name = et2.getText().toString();
                    if (name.equals(context.getString(R.string.empty))) {
                        showToast(context.getString(R.string.bad_request));
                    } else {
                        saveOnDisk(name);
                        dialog.dismiss();
                    }
                }
            });
            dialog.show();
        }
        else{
            showToast(context.getString(R.string.no_mem));
        }
    }

    @Click(R.id.share)
    protected void share(){
        shareMem();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SELECT_PHOTO && resultCode == RESULT_OK) {
            Uri uri = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
                memView.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void saveOnDisk(String name){
        try {
            String path = Environment.getExternalStorageDirectory().toString();
            OutputStream os = null;
            file = new File(path, name + context.getString(R.string.jpg));
            os = new FileOutputStream(file);
            if(currentImage == null){
                currentImage = bitmap;
            }
            currentImage.compress(Bitmap.CompressFormat.JPEG, 85, os);
            os.flush();
            os.close();
            MediaStore.Images.Media.insertImage(getContentResolver(), file.getAbsolutePath(), file.getName(), file.getName());
            showToast(context.getString(R.string.save_success));
            shareMemQuestion();
            share.setVisibility(View.VISIBLE);

        } catch (Exception e) {
            showToast(context.getString(R.string.save_fail));
        }
    }

    private void setListeners(){
        topText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() < 20 && bitmap != null) {
                    currentImage = drawText();
                    memView.setImageBitmap(currentImage);
                }
            }
        });
        bottomText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() < 20 && bitmap != null) {
                    currentImage = drawText();
                    memView.setImageBitmap(currentImage);
                }
            }
        });
    }

    private void shareMemQuestion(){
        DialogInterface.OnClickListener dialog = new DialogInterface.OnClickListener(){
            @Override
        public void onClick(DialogInterface dialog, int which){
                switch(which){
                    case DialogInterface.BUTTON_POSITIVE:
                        shareMem();
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(context.getString(R.string.question)).setPositiveButton(context.getString(R.string.yes), dialog).setNegativeButton(context.getString(R.string.no), dialog).show();

    }

    private void shareMem(){
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType(context.getString(R.string.image_jpeg));
        shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
        startActivity(Intent.createChooser(shareIntent, context.getString(R.string.share)));
    }

    private void addSpinners(){
        ArrayAdapter<CharSequence> topAdapter = ArrayAdapter.createFromResource(
                this, R.array.colorNames, android.R.layout.simple_spinner_item);
        topAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        topSpinner.setAdapter(topAdapter);

        ArrayAdapter<CharSequence> bottomAdapter = ArrayAdapter.createFromResource(
                this, R.array.colorNames, android.R.layout.simple_spinner_item);
        bottomAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        bottomSpinner.setAdapter(bottomAdapter);
    }

    private Bitmap drawText() {
        Bitmap b  = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(b);
        bitmap = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth(), bitmap.getHeight(), true);
        canvas.drawBitmap(bitmap, 0, 0, null);

        String[] colorNames = getResources().getStringArray(R.array.colorNames);
        int[] colors = getResources().getIntArray(R.array.listOfColors);
        String topColorSelected = topSpinner.getSelectedItem().toString();
        String bottomColorSelected = bottomSpinner.getSelectedItem().toString();

        int topIndex = 0;
        while(topIndex < colors.length && !colorNames[topIndex].equals(topColorSelected)){
            topIndex++;
        }

        int bottomIndex = 0;
        while(bottomIndex < colors.length && !colorNames[bottomIndex].equals(bottomColorSelected)){
            bottomIndex++;
        }

        Paint topPaint = new Paint(Paint.FAKE_BOLD_TEXT_FLAG);
        topPaint.setColor(colors[topIndex]);
        topPaint.setTextSize((int) (40));
        if(topText.getText().length() > 20)
            canvas.drawText(topText.getText().toString().substring(0,20), 10, 60, topPaint);
        else
            canvas.drawText(topText.getText().toString(), 10, 60, topPaint);
        Paint bottomPaint = new Paint(Paint.FAKE_BOLD_TEXT_FLAG);
        bottomPaint.setColor(colors[bottomIndex]);
        bottomPaint.setTextSize((int) (40));
        if(bottomText.getText().length() > 20)
        canvas.drawText(bottomText.getText().toString().substring(0,20), 10, (this.bitmap.getHeight() - 40), bottomPaint);
        else
            canvas.drawText(bottomText.getText().toString(), 10, (this.bitmap.getHeight() - 40), bottomPaint);

        return b;
    }

    private void showToast(String text){
        Toast.makeText(getApplicationContext(), text, Toast.LENGTH_LONG).show();
    }


}
